package com.springboot.hello.controller;

import com.springboot.hello.entity.SpringUser;
import com.springboot.hello.repository.SpringUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Map;

@Controller
@RequestMapping("/users")
public class SpringUserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringUserController.class);

    @Autowired
    private SpringUserRepository userRepository;

    @GetMapping(path = "/add")
    public @ResponseBody
    Map addNewUser(@RequestParam String name, @RequestParam String email) {
        LOGGER.info("entering addNewUser()");

        SpringUser n = new SpringUser();
        n.setName(name);
        n.setEmail(email);
        userRepository.save(n);

        return Collections.singletonMap("response", "user " + name + " saved");
    }

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<SpringUser> getAllUsers() {
        LOGGER.info("entering getAllUsers()");

        return userRepository.findAll();
    }
}
